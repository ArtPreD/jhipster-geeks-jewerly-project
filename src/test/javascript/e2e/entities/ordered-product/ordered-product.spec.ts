/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { OrderedProductComponentsPage, OrderedProductDeleteDialog, OrderedProductUpdatePage } from './ordered-product.page-object';

const expect = chai.expect;

describe('OrderedProduct e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let orderedProductUpdatePage: OrderedProductUpdatePage;
    let orderedProductComponentsPage: OrderedProductComponentsPage;
    let orderedProductDeleteDialog: OrderedProductDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load OrderedProducts', async () => {
        await navBarPage.goToEntity('ordered-product');
        orderedProductComponentsPage = new OrderedProductComponentsPage();
        await browser.wait(ec.visibilityOf(orderedProductComponentsPage.title), 5000);
        expect(await orderedProductComponentsPage.getTitle()).to.eq('geeksjewerlyApp.orderedProduct.home.title');
    });

    it('should load create OrderedProduct page', async () => {
        await orderedProductComponentsPage.clickOnCreateButton();
        orderedProductUpdatePage = new OrderedProductUpdatePage();
        expect(await orderedProductUpdatePage.getPageTitle()).to.eq('geeksjewerlyApp.orderedProduct.home.createOrEditLabel');
        await orderedProductUpdatePage.cancel();
    });

    it('should create and save OrderedProducts', async () => {
        const nbButtonsBeforeCreate = await orderedProductComponentsPage.countDeleteButtons();

        await orderedProductComponentsPage.clickOnCreateButton();
        await promise.all([
            orderedProductUpdatePage.setNameInput('name'),
            orderedProductUpdatePage.setArticleInput('article'),
            orderedProductUpdatePage.setSelectedSizeInput('selectedSize'),
            orderedProductUpdatePage.setColorInput('color'),
            orderedProductUpdatePage.setPriceInput('price'),
            orderedProductUpdatePage.setUrlInput('url'),
            orderedProductUpdatePage.productTypeSelectLastOption(),
            orderedProductUpdatePage.orderSelectLastOption()
        ]);
        expect(await orderedProductUpdatePage.getNameInput()).to.eq('name');
        expect(await orderedProductUpdatePage.getArticleInput()).to.eq('article');
        expect(await orderedProductUpdatePage.getSelectedSizeInput()).to.eq('selectedSize');
        expect(await orderedProductUpdatePage.getColorInput()).to.eq('color');
        expect(await orderedProductUpdatePage.getPriceInput()).to.eq('price');
        expect(await orderedProductUpdatePage.getUrlInput()).to.eq('url');
        await orderedProductUpdatePage.save();
        expect(await orderedProductUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await orderedProductComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last OrderedProduct', async () => {
        const nbButtonsBeforeDelete = await orderedProductComponentsPage.countDeleteButtons();
        await orderedProductComponentsPage.clickOnLastDeleteButton();

        orderedProductDeleteDialog = new OrderedProductDeleteDialog();
        expect(await orderedProductDeleteDialog.getDialogTitle()).to.eq('geeksjewerlyApp.orderedProduct.delete.question');
        await orderedProductDeleteDialog.clickOnConfirmButton();

        expect(await orderedProductComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
