import { element, by, ElementFinder } from 'protractor';

export class OrderedProductComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-ordered-product div table .btn-danger'));
    title = element.all(by.css('jhi-ordered-product div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class OrderedProductUpdatePage {
    pageTitle = element(by.id('jhi-ordered-product-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nameInput = element(by.id('field_name'));
    articleInput = element(by.id('field_article'));
    selectedSizeInput = element(by.id('field_selectedSize'));
    colorInput = element(by.id('field_color'));
    priceInput = element(by.id('field_price'));
    urlInput = element(by.id('field_url'));
    productTypeSelect = element(by.id('field_productType'));
    orderSelect = element(by.id('field_order'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setNameInput(name) {
        await this.nameInput.sendKeys(name);
    }

    async getNameInput() {
        return this.nameInput.getAttribute('value');
    }

    async setArticleInput(article) {
        await this.articleInput.sendKeys(article);
    }

    async getArticleInput() {
        return this.articleInput.getAttribute('value');
    }

    async setSelectedSizeInput(selectedSize) {
        await this.selectedSizeInput.sendKeys(selectedSize);
    }

    async getSelectedSizeInput() {
        return this.selectedSizeInput.getAttribute('value');
    }

    async setColorInput(color) {
        await this.colorInput.sendKeys(color);
    }

    async getColorInput() {
        return this.colorInput.getAttribute('value');
    }

    async setPriceInput(price) {
        await this.priceInput.sendKeys(price);
    }

    async getPriceInput() {
        return this.priceInput.getAttribute('value');
    }

    async setUrlInput(url) {
        await this.urlInput.sendKeys(url);
    }

    async getUrlInput() {
        return this.urlInput.getAttribute('value');
    }

    async setProductTypeSelect(productType) {
        await this.productTypeSelect.sendKeys(productType);
    }

    async getProductTypeSelect() {
        return this.productTypeSelect.element(by.css('option:checked')).getText();
    }

    async productTypeSelectLastOption() {
        await this.productTypeSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async orderSelectLastOption() {
        await this.orderSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async orderSelectOption(option) {
        await this.orderSelect.sendKeys(option);
    }

    getOrderSelect(): ElementFinder {
        return this.orderSelect;
    }

    async getOrderSelectedOption() {
        return this.orderSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class OrderedProductDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-orderedProduct-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-orderedProduct'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
