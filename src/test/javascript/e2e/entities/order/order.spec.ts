/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { OrderComponentsPage, OrderDeleteDialog, OrderUpdatePage } from './order.page-object';

const expect = chai.expect;

describe('Order e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let orderUpdatePage: OrderUpdatePage;
    let orderComponentsPage: OrderComponentsPage;
    let orderDeleteDialog: OrderDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Orders', async () => {
        await navBarPage.goToEntity('order');
        orderComponentsPage = new OrderComponentsPage();
        await browser.wait(ec.visibilityOf(orderComponentsPage.title), 5000);
        expect(await orderComponentsPage.getTitle()).to.eq('geeksjewerlyApp.order.home.title');
    });

    it('should load create Order page', async () => {
        await orderComponentsPage.clickOnCreateButton();
        orderUpdatePage = new OrderUpdatePage();
        expect(await orderUpdatePage.getPageTitle()).to.eq('geeksjewerlyApp.order.home.createOrEditLabel');
        await orderUpdatePage.cancel();
    });

    it('should create and save Orders', async () => {
        const nbButtonsBeforeCreate = await orderComponentsPage.countDeleteButtons();

        await orderComponentsPage.clickOnCreateButton();
        await promise.all([
            orderUpdatePage.setCustomerNameInput('customerName'),
            orderUpdatePage.setPhoneInput('phone'),
            orderUpdatePage.setEmailInput('email'),
            orderUpdatePage.setAddressInput('address'),
            orderUpdatePage.setOrderNumberInput('5'),
            orderUpdatePage.setCreateByInput('createBy'),
            orderUpdatePage.setCreateDateInput('2000-12-31'),
            orderUpdatePage.setModifyByInput('modifyBy'),
            orderUpdatePage.setModifyDateInput('2000-12-31'),
            orderUpdatePage.deliverTypeSelectLastOption(),
            orderUpdatePage.setTrackNumberInput('trackNumber'),
            orderUpdatePage.statusSelectLastOption()
        ]);
        expect(await orderUpdatePage.getCustomerNameInput()).to.eq('customerName');
        expect(await orderUpdatePage.getPhoneInput()).to.eq('phone');
        expect(await orderUpdatePage.getEmailInput()).to.eq('email');
        expect(await orderUpdatePage.getAddressInput()).to.eq('address');
        expect(await orderUpdatePage.getOrderNumberInput()).to.eq('5');
        expect(await orderUpdatePage.getCreateByInput()).to.eq('createBy');
        expect(await orderUpdatePage.getCreateDateInput()).to.eq('2000-12-31');
        expect(await orderUpdatePage.getModifyByInput()).to.eq('modifyBy');
        expect(await orderUpdatePage.getModifyDateInput()).to.eq('2000-12-31');
        expect(await orderUpdatePage.getTrackNumberInput()).to.eq('trackNumber');
        await orderUpdatePage.save();
        expect(await orderUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await orderComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Order', async () => {
        const nbButtonsBeforeDelete = await orderComponentsPage.countDeleteButtons();
        await orderComponentsPage.clickOnLastDeleteButton();

        orderDeleteDialog = new OrderDeleteDialog();
        expect(await orderDeleteDialog.getDialogTitle()).to.eq('geeksjewerlyApp.order.delete.question');
        await orderDeleteDialog.clickOnConfirmButton();

        expect(await orderComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
