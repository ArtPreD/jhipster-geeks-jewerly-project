import { element, by, ElementFinder } from 'protractor';

export class OrderComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-order div table .btn-danger'));
    title = element.all(by.css('jhi-order div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class OrderUpdatePage {
    pageTitle = element(by.id('jhi-order-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    customerNameInput = element(by.id('field_customerName'));
    phoneInput = element(by.id('field_phone'));
    emailInput = element(by.id('field_email'));
    addressInput = element(by.id('field_address'));
    orderNumberInput = element(by.id('field_orderNumber'));
    createByInput = element(by.id('field_createBy'));
    createDateInput = element(by.id('field_createDate'));
    modifyByInput = element(by.id('field_modifyBy'));
    modifyDateInput = element(by.id('field_modifyDate'));
    deliverTypeSelect = element(by.id('field_deliverType'));
    trackNumberInput = element(by.id('field_trackNumber'));
    statusSelect = element(by.id('field_status'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setCustomerNameInput(customerName) {
        await this.customerNameInput.sendKeys(customerName);
    }

    async getCustomerNameInput() {
        return this.customerNameInput.getAttribute('value');
    }

    async setPhoneInput(phone) {
        await this.phoneInput.sendKeys(phone);
    }

    async getPhoneInput() {
        return this.phoneInput.getAttribute('value');
    }

    async setEmailInput(email) {
        await this.emailInput.sendKeys(email);
    }

    async getEmailInput() {
        return this.emailInput.getAttribute('value');
    }

    async setAddressInput(address) {
        await this.addressInput.sendKeys(address);
    }

    async getAddressInput() {
        return this.addressInput.getAttribute('value');
    }

    async setOrderNumberInput(orderNumber) {
        await this.orderNumberInput.sendKeys(orderNumber);
    }

    async getOrderNumberInput() {
        return this.orderNumberInput.getAttribute('value');
    }

    async setCreateByInput(createBy) {
        await this.createByInput.sendKeys(createBy);
    }

    async getCreateByInput() {
        return this.createByInput.getAttribute('value');
    }

    async setCreateDateInput(createDate) {
        await this.createDateInput.sendKeys(createDate);
    }

    async getCreateDateInput() {
        return this.createDateInput.getAttribute('value');
    }

    async setModifyByInput(modifyBy) {
        await this.modifyByInput.sendKeys(modifyBy);
    }

    async getModifyByInput() {
        return this.modifyByInput.getAttribute('value');
    }

    async setModifyDateInput(modifyDate) {
        await this.modifyDateInput.sendKeys(modifyDate);
    }

    async getModifyDateInput() {
        return this.modifyDateInput.getAttribute('value');
    }

    async setDeliverTypeSelect(deliverType) {
        await this.deliverTypeSelect.sendKeys(deliverType);
    }

    async getDeliverTypeSelect() {
        return this.deliverTypeSelect.element(by.css('option:checked')).getText();
    }

    async deliverTypeSelectLastOption() {
        await this.deliverTypeSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setTrackNumberInput(trackNumber) {
        await this.trackNumberInput.sendKeys(trackNumber);
    }

    async getTrackNumberInput() {
        return this.trackNumberInput.getAttribute('value');
    }

    async setStatusSelect(status) {
        await this.statusSelect.sendKeys(status);
    }

    async getStatusSelect() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    async statusSelectLastOption() {
        await this.statusSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class OrderDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-order-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-order'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
