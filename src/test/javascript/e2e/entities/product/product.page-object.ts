import { element, by, ElementFinder } from 'protractor';

export class ProductComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-product div table .btn-danger'));
    title = element.all(by.css('jhi-product div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ProductUpdatePage {
    pageTitle = element(by.id('jhi-product-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    productTypeSelect = element(by.id('field_productType'));
    nameInput = element(by.id('field_name'));
    articleInput = element(by.id('field_article'));
    minSizeInput = element(by.id('field_minSize'));
    maxSizeInput = element(by.id('field_maxSize'));
    sizesInput = element(by.id('field_sizes'));
    rockSelect = element(by.id('field_rock'));
    materialSelect = element(by.id('field_material'));
    colorSelect = element(by.id('field_color'));
    categoryIdInput = element(by.id('field_categoryId'));
    availableInput = element(by.id('field_available'));
    friendlyUrlInput = element(by.id('field_friendlyUrl'));
    priceInput = element(by.id('field_price'));
    metaInput = element(by.id('field_meta'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setProductTypeSelect(productType) {
        await this.productTypeSelect.sendKeys(productType);
    }

    async getProductTypeSelect() {
        return this.productTypeSelect.element(by.css('option:checked')).getText();
    }

    async productTypeSelectLastOption() {
        await this.productTypeSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setNameInput(name) {
        await this.nameInput.sendKeys(name);
    }

    async getNameInput() {
        return this.nameInput.getAttribute('value');
    }

    async setArticleInput(article) {
        await this.articleInput.sendKeys(article);
    }

    async getArticleInput() {
        return this.articleInput.getAttribute('value');
    }

    async setMinSizeInput(minSize) {
        await this.minSizeInput.sendKeys(minSize);
    }

    async getMinSizeInput() {
        return this.minSizeInput.getAttribute('value');
    }

    async setMaxSizeInput(maxSize) {
        await this.maxSizeInput.sendKeys(maxSize);
    }

    async getMaxSizeInput() {
        return this.maxSizeInput.getAttribute('value');
    }

    async setSizesInput(sizes) {
        await this.sizesInput.sendKeys(sizes);
    }

    async getSizesInput() {
        return this.sizesInput.getAttribute('value');
    }

    async setRockSelect(rock) {
        await this.rockSelect.sendKeys(rock);
    }

    async getRockSelect() {
        return this.rockSelect.element(by.css('option:checked')).getText();
    }

    async rockSelectLastOption() {
        await this.rockSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setMaterialSelect(material) {
        await this.materialSelect.sendKeys(material);
    }

    async getMaterialSelect() {
        return this.materialSelect.element(by.css('option:checked')).getText();
    }

    async materialSelectLastOption() {
        await this.materialSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setColorSelect(color) {
        await this.colorSelect.sendKeys(color);
    }

    async getColorSelect() {
        return this.colorSelect.element(by.css('option:checked')).getText();
    }

    async colorSelectLastOption() {
        await this.colorSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setCategoryIdInput(categoryId) {
        await this.categoryIdInput.sendKeys(categoryId);
    }

    async getCategoryIdInput() {
        return this.categoryIdInput.getAttribute('value');
    }

    getAvailableInput() {
        return this.availableInput;
    }
    async setFriendlyUrlInput(friendlyUrl) {
        await this.friendlyUrlInput.sendKeys(friendlyUrl);
    }

    async getFriendlyUrlInput() {
        return this.friendlyUrlInput.getAttribute('value');
    }

    async setPriceInput(price) {
        await this.priceInput.sendKeys(price);
    }

    async getPriceInput() {
        return this.priceInput.getAttribute('value');
    }

    async setMetaInput(meta) {
        await this.metaInput.sendKeys(meta);
    }

    async getMetaInput() {
        return this.metaInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ProductDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-product-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-product'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
