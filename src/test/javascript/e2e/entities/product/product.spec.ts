/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ProductComponentsPage, ProductDeleteDialog, ProductUpdatePage } from './product.page-object';

const expect = chai.expect;

describe('Product e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let productUpdatePage: ProductUpdatePage;
    let productComponentsPage: ProductComponentsPage;
    let productDeleteDialog: ProductDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Products', async () => {
        await navBarPage.goToEntity('product');
        productComponentsPage = new ProductComponentsPage();
        await browser.wait(ec.visibilityOf(productComponentsPage.title), 5000);
        expect(await productComponentsPage.getTitle()).to.eq('geeksjewerlyApp.product.home.title');
    });

    it('should load create Product page', async () => {
        await productComponentsPage.clickOnCreateButton();
        productUpdatePage = new ProductUpdatePage();
        expect(await productUpdatePage.getPageTitle()).to.eq('geeksjewerlyApp.product.home.createOrEditLabel');
        await productUpdatePage.cancel();
    });

    it('should create and save Products', async () => {
        const nbButtonsBeforeCreate = await productComponentsPage.countDeleteButtons();

        await productComponentsPage.clickOnCreateButton();
        await promise.all([
            productUpdatePage.productTypeSelectLastOption(),
            productUpdatePage.setNameInput('name'),
            productUpdatePage.setArticleInput('article'),
            productUpdatePage.setMinSizeInput('5'),
            productUpdatePage.setMaxSizeInput('5'),
            productUpdatePage.setSizesInput('sizes'),
            productUpdatePage.rockSelectLastOption(),
            productUpdatePage.materialSelectLastOption(),
            productUpdatePage.colorSelectLastOption(),
            productUpdatePage.setCategoryIdInput('5'),
            productUpdatePage.setFriendlyUrlInput('friendlyUrl'),
            productUpdatePage.setPriceInput('5'),
            productUpdatePage.setMetaInput('meta')
        ]);
        expect(await productUpdatePage.getNameInput()).to.eq('name');
        expect(await productUpdatePage.getArticleInput()).to.eq('article');
        expect(await productUpdatePage.getMinSizeInput()).to.eq('5');
        expect(await productUpdatePage.getMaxSizeInput()).to.eq('5');
        expect(await productUpdatePage.getSizesInput()).to.eq('sizes');
        expect(await productUpdatePage.getCategoryIdInput()).to.eq('5');
        const selectedAvailable = productUpdatePage.getAvailableInput();
        if (await selectedAvailable.isSelected()) {
            await productUpdatePage.getAvailableInput().click();
            expect(await productUpdatePage.getAvailableInput().isSelected()).to.be.false;
        } else {
            await productUpdatePage.getAvailableInput().click();
            expect(await productUpdatePage.getAvailableInput().isSelected()).to.be.true;
        }
        expect(await productUpdatePage.getFriendlyUrlInput()).to.eq('friendlyUrl');
        expect(await productUpdatePage.getPriceInput()).to.eq('5');
        expect(await productUpdatePage.getMetaInput()).to.eq('meta');
        await productUpdatePage.save();
        expect(await productUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await productComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Product', async () => {
        const nbButtonsBeforeDelete = await productComponentsPage.countDeleteButtons();
        await productComponentsPage.clickOnLastDeleteButton();

        productDeleteDialog = new ProductDeleteDialog();
        expect(await productDeleteDialog.getDialogTitle()).to.eq('geeksjewerlyApp.product.delete.question');
        await productDeleteDialog.clickOnConfirmButton();

        expect(await productComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
