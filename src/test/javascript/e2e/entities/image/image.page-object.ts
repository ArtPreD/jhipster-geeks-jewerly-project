import { element, by, ElementFinder } from 'protractor';

export class ImageComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-image div table .btn-danger'));
    title = element.all(by.css('jhi-image div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ImageUpdatePage {
    pageTitle = element(by.id('jhi-image-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    originalNameInput = element(by.id('field_originalName'));
    urlInput = element(by.id('field_url'));
    sizeInput = element(by.id('field_size'));
    publicIdInput = element(by.id('field_publicId'));
    versionInput = element(by.id('field_version'));
    metaInput = element(by.id('field_meta'));
    mainInput = element(by.id('field_main'));
    productSelect = element(by.id('field_product'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setOriginalNameInput(originalName) {
        await this.originalNameInput.sendKeys(originalName);
    }

    async getOriginalNameInput() {
        return this.originalNameInput.getAttribute('value');
    }

    async setUrlInput(url) {
        await this.urlInput.sendKeys(url);
    }

    async getUrlInput() {
        return this.urlInput.getAttribute('value');
    }

    async setSizeInput(size) {
        await this.sizeInput.sendKeys(size);
    }

    async getSizeInput() {
        return this.sizeInput.getAttribute('value');
    }

    async setPublicIdInput(publicId) {
        await this.publicIdInput.sendKeys(publicId);
    }

    async getPublicIdInput() {
        return this.publicIdInput.getAttribute('value');
    }

    async setVersionInput(version) {
        await this.versionInput.sendKeys(version);
    }

    async getVersionInput() {
        return this.versionInput.getAttribute('value');
    }

    async setMetaInput(meta) {
        await this.metaInput.sendKeys(meta);
    }

    async getMetaInput() {
        return this.metaInput.getAttribute('value');
    }

    getMainInput() {
        return this.mainInput;
    }

    async productSelectLastOption() {
        await this.productSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async productSelectOption(option) {
        await this.productSelect.sendKeys(option);
    }

    getProductSelect(): ElementFinder {
        return this.productSelect;
    }

    async getProductSelectedOption() {
        return this.productSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ImageDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-image-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-image'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
