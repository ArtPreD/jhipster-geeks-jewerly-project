/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ImageComponentsPage, ImageDeleteDialog, ImageUpdatePage } from './image.page-object';

const expect = chai.expect;

describe('Image e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let imageUpdatePage: ImageUpdatePage;
    let imageComponentsPage: ImageComponentsPage;
    let imageDeleteDialog: ImageDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Images', async () => {
        await navBarPage.goToEntity('image');
        imageComponentsPage = new ImageComponentsPage();
        await browser.wait(ec.visibilityOf(imageComponentsPage.title), 5000);
        expect(await imageComponentsPage.getTitle()).to.eq('geeksjewerlyApp.image.home.title');
    });

    it('should load create Image page', async () => {
        await imageComponentsPage.clickOnCreateButton();
        imageUpdatePage = new ImageUpdatePage();
        expect(await imageUpdatePage.getPageTitle()).to.eq('geeksjewerlyApp.image.home.createOrEditLabel');
        await imageUpdatePage.cancel();
    });

    it('should create and save Images', async () => {
        const nbButtonsBeforeCreate = await imageComponentsPage.countDeleteButtons();

        await imageComponentsPage.clickOnCreateButton();
        await promise.all([
            imageUpdatePage.setOriginalNameInput('originalName'),
            imageUpdatePage.setUrlInput('url'),
            imageUpdatePage.setSizeInput('5'),
            imageUpdatePage.setPublicIdInput('publicId'),
            imageUpdatePage.setVersionInput('version'),
            imageUpdatePage.setMetaInput('meta'),
            imageUpdatePage.productSelectLastOption()
        ]);
        expect(await imageUpdatePage.getOriginalNameInput()).to.eq('originalName');
        expect(await imageUpdatePage.getUrlInput()).to.eq('url');
        expect(await imageUpdatePage.getSizeInput()).to.eq('5');
        expect(await imageUpdatePage.getPublicIdInput()).to.eq('publicId');
        expect(await imageUpdatePage.getVersionInput()).to.eq('version');
        expect(await imageUpdatePage.getMetaInput()).to.eq('meta');
        const selectedMain = imageUpdatePage.getMainInput();
        if (await selectedMain.isSelected()) {
            await imageUpdatePage.getMainInput().click();
            expect(await imageUpdatePage.getMainInput().isSelected()).to.be.false;
        } else {
            await imageUpdatePage.getMainInput().click();
            expect(await imageUpdatePage.getMainInput().isSelected()).to.be.true;
        }
        await imageUpdatePage.save();
        expect(await imageUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await imageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Image', async () => {
        const nbButtonsBeforeDelete = await imageComponentsPage.countDeleteButtons();
        await imageComponentsPage.clickOnLastDeleteButton();

        imageDeleteDialog = new ImageDeleteDialog();
        expect(await imageDeleteDialog.getDialogTitle()).to.eq('geeksjewerlyApp.image.delete.question');
        await imageDeleteDialog.clickOnConfirmButton();

        expect(await imageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
