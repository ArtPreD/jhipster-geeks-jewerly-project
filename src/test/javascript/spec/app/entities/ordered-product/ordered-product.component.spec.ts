/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GeeksjewerlyTestModule } from '../../../test.module';
import { OrderedProductComponent } from 'app/entities/ordered-product/ordered-product.component';
import { OrderedProductService } from 'app/entities/ordered-product/ordered-product.service';
import { OrderedProduct } from 'app/shared/model/ordered-product.model';

describe('Component Tests', () => {
    describe('OrderedProduct Management Component', () => {
        let comp: OrderedProductComponent;
        let fixture: ComponentFixture<OrderedProductComponent>;
        let service: OrderedProductService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GeeksjewerlyTestModule],
                declarations: [OrderedProductComponent],
                providers: []
            })
                .overrideTemplate(OrderedProductComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(OrderedProductComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OrderedProductService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new OrderedProduct(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.orderedProducts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
