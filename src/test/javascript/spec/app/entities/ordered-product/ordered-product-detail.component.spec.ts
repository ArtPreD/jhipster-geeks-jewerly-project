/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GeeksjewerlyTestModule } from '../../../test.module';
import { OrderedProductDetailComponent } from 'app/entities/ordered-product/ordered-product-detail.component';
import { OrderedProduct } from 'app/shared/model/ordered-product.model';

describe('Component Tests', () => {
    describe('OrderedProduct Management Detail Component', () => {
        let comp: OrderedProductDetailComponent;
        let fixture: ComponentFixture<OrderedProductDetailComponent>;
        const route = ({ data: of({ orderedProduct: new OrderedProduct(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GeeksjewerlyTestModule],
                declarations: [OrderedProductDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(OrderedProductDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(OrderedProductDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.orderedProduct).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
