/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GeeksjewerlyTestModule } from '../../../test.module';
import { OrderedProductUpdateComponent } from 'app/entities/ordered-product/ordered-product-update.component';
import { OrderedProductService } from 'app/entities/ordered-product/ordered-product.service';
import { OrderedProduct } from 'app/shared/model/ordered-product.model';

describe('Component Tests', () => {
    describe('OrderedProduct Management Update Component', () => {
        let comp: OrderedProductUpdateComponent;
        let fixture: ComponentFixture<OrderedProductUpdateComponent>;
        let service: OrderedProductService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GeeksjewerlyTestModule],
                declarations: [OrderedProductUpdateComponent]
            })
                .overrideTemplate(OrderedProductUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(OrderedProductUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OrderedProductService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new OrderedProduct(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.orderedProduct = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new OrderedProduct();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.orderedProduct = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
