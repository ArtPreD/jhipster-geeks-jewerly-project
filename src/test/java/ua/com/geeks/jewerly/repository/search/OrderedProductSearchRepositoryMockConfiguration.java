package ua.com.geeks.jewerly.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of OrderedProductSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class OrderedProductSearchRepositoryMockConfiguration {

    @MockBean
    private OrderedProductSearchRepository mockOrderedProductSearchRepository;

}
