package ua.com.geeks.jewerly.web.rest;

import ua.com.geeks.jewerly.GeeksjewerlyApp;

import ua.com.geeks.jewerly.domain.OrderedProduct;
import ua.com.geeks.jewerly.repository.OrderedProductRepository;
import ua.com.geeks.jewerly.repository.search.OrderedProductSearchRepository;
import ua.com.geeks.jewerly.service.OrderedProductService;
import ua.com.geeks.jewerly.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;


import static ua.com.geeks.jewerly.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ua.com.geeks.jewerly.domain.enumeration.ProductType;
/**
 * Test class for the OrderedProductResource REST controller.
 *
 * @see OrderedProductResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GeeksjewerlyApp.class)
public class OrderedProductResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ARTICLE = "AAAAAAAAAA";
    private static final String UPDATED_ARTICLE = "BBBBBBBBBB";

    private static final String DEFAULT_SELECTED_SIZE = "AAAAAAAAAA";
    private static final String UPDATED_SELECTED_SIZE = "BBBBBBBBBB";

    private static final String DEFAULT_COLOR = "AAAAAAAAAA";
    private static final String UPDATED_COLOR = "BBBBBBBBBB";

    private static final String DEFAULT_PRICE = "AAAAAAAAAA";
    private static final String UPDATED_PRICE = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final ProductType DEFAULT_PRODUCT_TYPE = ProductType.PETIT;
    private static final ProductType UPDATED_PRODUCT_TYPE = ProductType.PUSET;

    @Autowired
    private OrderedProductRepository orderedProductRepository;

    @Autowired
    private OrderedProductService orderedProductService;

    /**
     * This repository is mocked in the ua.com.geeks.jewerly.repository.search test package.
     *
     * @see ua.com.geeks.jewerly.repository.search.OrderedProductSearchRepositoryMockConfiguration
     */
    @Autowired
    private OrderedProductSearchRepository mockOrderedProductSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrderedProductMockMvc;

    private OrderedProduct orderedProduct;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrderedProductResource orderedProductResource = new OrderedProductResource(orderedProductService);
        this.restOrderedProductMockMvc = MockMvcBuilders.standaloneSetup(orderedProductResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderedProduct createEntity(EntityManager em) {
        OrderedProduct orderedProduct = new OrderedProduct()
            .name(DEFAULT_NAME)
            .article(DEFAULT_ARTICLE)
            .selectedSize(DEFAULT_SELECTED_SIZE)
            .color(DEFAULT_COLOR)
            .price(DEFAULT_PRICE)
            .url(DEFAULT_URL)
            .productType(DEFAULT_PRODUCT_TYPE);
        return orderedProduct;
    }

    @Before
    public void initTest() {
        orderedProduct = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrderedProduct() throws Exception {
        int databaseSizeBeforeCreate = orderedProductRepository.findAll().size();

        // Create the OrderedProduct
        restOrderedProductMockMvc.perform(post("/api/ordered-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderedProduct)))
            .andExpect(status().isCreated());

        // Validate the OrderedProduct in the database
        List<OrderedProduct> orderedProductList = orderedProductRepository.findAll();
        assertThat(orderedProductList).hasSize(databaseSizeBeforeCreate + 1);
        OrderedProduct testOrderedProduct = orderedProductList.get(orderedProductList.size() - 1);
        assertThat(testOrderedProduct.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testOrderedProduct.getArticle()).isEqualTo(DEFAULT_ARTICLE);
        assertThat(testOrderedProduct.getSelectedSize()).isEqualTo(DEFAULT_SELECTED_SIZE);
        assertThat(testOrderedProduct.getColor()).isEqualTo(DEFAULT_COLOR);
        assertThat(testOrderedProduct.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testOrderedProduct.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testOrderedProduct.getProductType()).isEqualTo(DEFAULT_PRODUCT_TYPE);

        // Validate the OrderedProduct in Elasticsearch
        verify(mockOrderedProductSearchRepository, times(1)).save(testOrderedProduct);
    }

    @Test
    @Transactional
    public void createOrderedProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderedProductRepository.findAll().size();

        // Create the OrderedProduct with an existing ID
        orderedProduct.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderedProductMockMvc.perform(post("/api/ordered-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderedProduct)))
            .andExpect(status().isBadRequest());

        // Validate the OrderedProduct in the database
        List<OrderedProduct> orderedProductList = orderedProductRepository.findAll();
        assertThat(orderedProductList).hasSize(databaseSizeBeforeCreate);

        // Validate the OrderedProduct in Elasticsearch
        verify(mockOrderedProductSearchRepository, times(0)).save(orderedProduct);
    }

    @Test
    @Transactional
    public void getAllOrderedProducts() throws Exception {
        // Initialize the database
        orderedProductRepository.saveAndFlush(orderedProduct);

        // Get all the orderedProductList
        restOrderedProductMockMvc.perform(get("/api/ordered-products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderedProduct.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].article").value(hasItem(DEFAULT_ARTICLE.toString())))
            .andExpect(jsonPath("$.[*].selectedSize").value(hasItem(DEFAULT_SELECTED_SIZE.toString())))
            .andExpect(jsonPath("$.[*].color").value(hasItem(DEFAULT_COLOR.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].productType").value(hasItem(DEFAULT_PRODUCT_TYPE.toString())));
    }
    
    @Test
    @Transactional
    public void getOrderedProduct() throws Exception {
        // Initialize the database
        orderedProductRepository.saveAndFlush(orderedProduct);

        // Get the orderedProduct
        restOrderedProductMockMvc.perform(get("/api/ordered-products/{id}", orderedProduct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(orderedProduct.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.article").value(DEFAULT_ARTICLE.toString()))
            .andExpect(jsonPath("$.selectedSize").value(DEFAULT_SELECTED_SIZE.toString()))
            .andExpect(jsonPath("$.color").value(DEFAULT_COLOR.toString()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.productType").value(DEFAULT_PRODUCT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOrderedProduct() throws Exception {
        // Get the orderedProduct
        restOrderedProductMockMvc.perform(get("/api/ordered-products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderedProduct() throws Exception {
        // Initialize the database
        orderedProductService.save(orderedProduct);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockOrderedProductSearchRepository);

        int databaseSizeBeforeUpdate = orderedProductRepository.findAll().size();

        // Update the orderedProduct
        OrderedProduct updatedOrderedProduct = orderedProductRepository.findById(orderedProduct.getId()).get();
        // Disconnect from session so that the updates on updatedOrderedProduct are not directly saved in db
        em.detach(updatedOrderedProduct);
        updatedOrderedProduct
            .name(UPDATED_NAME)
            .article(UPDATED_ARTICLE)
            .selectedSize(UPDATED_SELECTED_SIZE)
            .color(UPDATED_COLOR)
            .price(UPDATED_PRICE)
            .url(UPDATED_URL)
            .productType(UPDATED_PRODUCT_TYPE);

        restOrderedProductMockMvc.perform(put("/api/ordered-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOrderedProduct)))
            .andExpect(status().isOk());

        // Validate the OrderedProduct in the database
        List<OrderedProduct> orderedProductList = orderedProductRepository.findAll();
        assertThat(orderedProductList).hasSize(databaseSizeBeforeUpdate);
        OrderedProduct testOrderedProduct = orderedProductList.get(orderedProductList.size() - 1);
        assertThat(testOrderedProduct.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testOrderedProduct.getArticle()).isEqualTo(UPDATED_ARTICLE);
        assertThat(testOrderedProduct.getSelectedSize()).isEqualTo(UPDATED_SELECTED_SIZE);
        assertThat(testOrderedProduct.getColor()).isEqualTo(UPDATED_COLOR);
        assertThat(testOrderedProduct.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testOrderedProduct.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testOrderedProduct.getProductType()).isEqualTo(UPDATED_PRODUCT_TYPE);

        // Validate the OrderedProduct in Elasticsearch
        verify(mockOrderedProductSearchRepository, times(1)).save(testOrderedProduct);
    }

    @Test
    @Transactional
    public void updateNonExistingOrderedProduct() throws Exception {
        int databaseSizeBeforeUpdate = orderedProductRepository.findAll().size();

        // Create the OrderedProduct

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderedProductMockMvc.perform(put("/api/ordered-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderedProduct)))
            .andExpect(status().isBadRequest());

        // Validate the OrderedProduct in the database
        List<OrderedProduct> orderedProductList = orderedProductRepository.findAll();
        assertThat(orderedProductList).hasSize(databaseSizeBeforeUpdate);

        // Validate the OrderedProduct in Elasticsearch
        verify(mockOrderedProductSearchRepository, times(0)).save(orderedProduct);
    }

    @Test
    @Transactional
    public void deleteOrderedProduct() throws Exception {
        // Initialize the database
        orderedProductService.save(orderedProduct);

        int databaseSizeBeforeDelete = orderedProductRepository.findAll().size();

        // Delete the orderedProduct
        restOrderedProductMockMvc.perform(delete("/api/ordered-products/{id}", orderedProduct.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<OrderedProduct> orderedProductList = orderedProductRepository.findAll();
        assertThat(orderedProductList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the OrderedProduct in Elasticsearch
        verify(mockOrderedProductSearchRepository, times(1)).deleteById(orderedProduct.getId());
    }

    @Test
    @Transactional
    public void searchOrderedProduct() throws Exception {
        // Initialize the database
        orderedProductService.save(orderedProduct);
        when(mockOrderedProductSearchRepository.search(queryStringQuery("id:" + orderedProduct.getId())))
            .thenReturn(Collections.singletonList(orderedProduct));
        // Search the orderedProduct
        restOrderedProductMockMvc.perform(get("/api/_search/ordered-products?query=id:" + orderedProduct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderedProduct.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].article").value(hasItem(DEFAULT_ARTICLE)))
            .andExpect(jsonPath("$.[*].selectedSize").value(hasItem(DEFAULT_SELECTED_SIZE)))
            .andExpect(jsonPath("$.[*].color").value(hasItem(DEFAULT_COLOR)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
            .andExpect(jsonPath("$.[*].productType").value(hasItem(DEFAULT_PRODUCT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderedProduct.class);
        OrderedProduct orderedProduct1 = new OrderedProduct();
        orderedProduct1.setId(1L);
        OrderedProduct orderedProduct2 = new OrderedProduct();
        orderedProduct2.setId(orderedProduct1.getId());
        assertThat(orderedProduct1).isEqualTo(orderedProduct2);
        orderedProduct2.setId(2L);
        assertThat(orderedProduct1).isNotEqualTo(orderedProduct2);
        orderedProduct1.setId(null);
        assertThat(orderedProduct1).isNotEqualTo(orderedProduct2);
    }
}
