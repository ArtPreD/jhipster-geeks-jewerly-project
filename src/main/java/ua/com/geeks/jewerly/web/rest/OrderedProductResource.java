package ua.com.geeks.jewerly.web.rest;
import ua.com.geeks.jewerly.domain.OrderedProduct;
import ua.com.geeks.jewerly.service.OrderedProductService;
import ua.com.geeks.jewerly.web.rest.errors.BadRequestAlertException;
import ua.com.geeks.jewerly.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing OrderedProduct.
 */
@RestController
@RequestMapping("/api")
public class OrderedProductResource {

    private final Logger log = LoggerFactory.getLogger(OrderedProductResource.class);

    private static final String ENTITY_NAME = "orderedProduct";

    private final OrderedProductService orderedProductService;

    public OrderedProductResource(OrderedProductService orderedProductService) {
        this.orderedProductService = orderedProductService;
    }

    /**
     * POST  /ordered-products : Create a new orderedProduct.
     *
     * @param orderedProduct the orderedProduct to create
     * @return the ResponseEntity with status 201 (Created) and with body the new orderedProduct, or with status 400 (Bad Request) if the orderedProduct has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ordered-products")
    public ResponseEntity<OrderedProduct> createOrderedProduct(@RequestBody OrderedProduct orderedProduct) throws URISyntaxException {
        log.debug("REST request to save OrderedProduct : {}", orderedProduct);
        if (orderedProduct.getId() != null) {
            throw new BadRequestAlertException("A new orderedProduct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderedProduct result = orderedProductService.save(orderedProduct);
        return ResponseEntity.created(new URI("/api/ordered-products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ordered-products : Updates an existing orderedProduct.
     *
     * @param orderedProduct the orderedProduct to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated orderedProduct,
     * or with status 400 (Bad Request) if the orderedProduct is not valid,
     * or with status 500 (Internal Server Error) if the orderedProduct couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ordered-products")
    public ResponseEntity<OrderedProduct> updateOrderedProduct(@RequestBody OrderedProduct orderedProduct) throws URISyntaxException {
        log.debug("REST request to update OrderedProduct : {}", orderedProduct);
        if (orderedProduct.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrderedProduct result = orderedProductService.save(orderedProduct);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, orderedProduct.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ordered-products : get all the orderedProducts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of orderedProducts in body
     */
    @GetMapping("/ordered-products")
    public List<OrderedProduct> getAllOrderedProducts() {
        log.debug("REST request to get all OrderedProducts");
        return orderedProductService.findAll();
    }

    /**
     * GET  /ordered-products/:id : get the "id" orderedProduct.
     *
     * @param id the id of the orderedProduct to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the orderedProduct, or with status 404 (Not Found)
     */
    @GetMapping("/ordered-products/{id}")
    public ResponseEntity<OrderedProduct> getOrderedProduct(@PathVariable Long id) {
        log.debug("REST request to get OrderedProduct : {}", id);
        Optional<OrderedProduct> orderedProduct = orderedProductService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderedProduct);
    }

    /**
     * DELETE  /ordered-products/:id : delete the "id" orderedProduct.
     *
     * @param id the id of the orderedProduct to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ordered-products/{id}")
    public ResponseEntity<Void> deleteOrderedProduct(@PathVariable Long id) {
        log.debug("REST request to delete OrderedProduct : {}", id);
        orderedProductService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/ordered-products?query=:query : search for the orderedProduct corresponding
     * to the query.
     *
     * @param query the query of the orderedProduct search
     * @return the result of the search
     */
    @GetMapping("/_search/ordered-products")
    public List<OrderedProduct> searchOrderedProducts(@RequestParam String query) {
        log.debug("REST request to search OrderedProducts for query {}", query);
        return orderedProductService.search(query);
    }

}
