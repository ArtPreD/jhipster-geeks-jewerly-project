/**
 * View Models used by Spring MVC REST controllers.
 */
package ua.com.geeks.jewerly.web.rest.vm;
