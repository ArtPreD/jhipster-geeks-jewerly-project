/**
 * Data Access Objects used by WebSocket services.
 */
package ua.com.geeks.jewerly.web.websocket.dto;
