package ua.com.geeks.jewerly.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(ua.com.geeks.jewerly.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Order.class.getName(), jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Order.class.getName() + ".products", jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Order.class.getName() + ".comments", jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.OrderedProduct.class.getName(), jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Image.class.getName(), jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Comment.class.getName(), jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Category.class.getName(), jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Category.class.getName() + ".subcategories", jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Product.class.getName(), jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Product.class.getName() + ".images", jcacheConfiguration);
            cm.createCache(ua.com.geeks.jewerly.domain.Product.class.getName() + ".comments", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
