package ua.com.geeks.jewerly.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import ua.com.geeks.jewerly.domain.enumeration.DeliverType;

import ua.com.geeks.jewerly.domain.enumeration.OrderStatus;

/**
 * A Order.
 */
@Entity
@Table(name = "jhi_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "customer_name", nullable = false)
    private String customerName;

    @NotNull
    @Column(name = "phone", nullable = false)
    private String phone;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "address", nullable = false)
    private String address;

    @NotNull
    @Column(name = "order_number", nullable = false, unique = true)
    private Long orderNumber;

    @NotNull
    @Column(name = "create_by", nullable = false)
    private String createBy;

    @NotNull
    @Column(name = "create_date", nullable = false)
    private LocalDate createDate;

    @NotNull
    @Column(name = "modify_by", nullable = false)
    private String modifyBy;

    @NotNull
    @Column(name = "modify_date", nullable = false)
    private LocalDate modifyDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "deliver_type", nullable = false)
    private DeliverType deliverType;

    @Column(name = "track_number")
    private String trackNumber;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private OrderStatus status;

    @OneToMany(mappedBy = "order")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OrderedProduct> products = new HashSet<>();
    @OneToMany(mappedBy = "order")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Comment> comments = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public Order customerName(String customerName) {
        this.customerName = customerName;
        return this;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhone() {
        return phone;
    }

    public Order phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public Order email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public Order address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public Order orderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCreateBy() {
        return createBy;
    }

    public Order createBy(String createBy) {
        this.createBy = createBy;
        return this;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public Order createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public Order modifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
        return this;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public LocalDate getModifyDate() {
        return modifyDate;
    }

    public Order modifyDate(LocalDate modifyDate) {
        this.modifyDate = modifyDate;
        return this;
    }

    public void setModifyDate(LocalDate modifyDate) {
        this.modifyDate = modifyDate;
    }

    public DeliverType getDeliverType() {
        return deliverType;
    }

    public Order deliverType(DeliverType deliverType) {
        this.deliverType = deliverType;
        return this;
    }

    public void setDeliverType(DeliverType deliverType) {
        this.deliverType = deliverType;
    }

    public String getTrackNumber() {
        return trackNumber;
    }

    public Order trackNumber(String trackNumber) {
        this.trackNumber = trackNumber;
        return this;
    }

    public void setTrackNumber(String trackNumber) {
        this.trackNumber = trackNumber;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public Order status(OrderStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Set<OrderedProduct> getProducts() {
        return products;
    }

    public Order products(Set<OrderedProduct> orderedProducts) {
        this.products = orderedProducts;
        return this;
    }

    public Order addProducts(OrderedProduct orderedProduct) {
        this.products.add(orderedProduct);
        orderedProduct.setOrder(this);
        return this;
    }

    public Order removeProducts(OrderedProduct orderedProduct) {
        this.products.remove(orderedProduct);
        orderedProduct.setOrder(null);
        return this;
    }

    public void setProducts(Set<OrderedProduct> orderedProducts) {
        this.products = orderedProducts;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Order comments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public Order addComments(Comment comment) {
        this.comments.add(comment);
        comment.setOrder(this);
        return this;
    }

    public Order removeComments(Comment comment) {
        this.comments.remove(comment);
        comment.setOrder(null);
        return this;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Order order = (Order) o;
        if (order.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), order.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Order{" +
            "id=" + getId() +
            ", customerName='" + getCustomerName() + "'" +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", address='" + getAddress() + "'" +
            ", orderNumber=" + getOrderNumber() +
            ", createBy='" + getCreateBy() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", modifyBy='" + getModifyBy() + "'" +
            ", modifyDate='" + getModifyDate() + "'" +
            ", deliverType='" + getDeliverType() + "'" +
            ", trackNumber='" + getTrackNumber() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
