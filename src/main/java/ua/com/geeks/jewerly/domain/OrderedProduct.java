package ua.com.geeks.jewerly.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

import ua.com.geeks.jewerly.domain.enumeration.ProductType;

/**
 * A OrderedProduct.
 */
@Entity
@Table(name = "ordered_product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "orderedproduct")
public class OrderedProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "article")
    private String article;

    @Column(name = "selected_size")
    private String selectedSize;

    @Column(name = "color")
    private String color;

    @Column(name = "price")
    private String price;

    @Column(name = "url")
    private String url;

    @Enumerated(EnumType.STRING)
    @Column(name = "product_type")
    private ProductType productType;

    @ManyToOne
    @JsonIgnoreProperties("products")
    private Order order;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public OrderedProduct name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArticle() {
        return article;
    }

    public OrderedProduct article(String article) {
        this.article = article;
        return this;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getSelectedSize() {
        return selectedSize;
    }

    public OrderedProduct selectedSize(String selectedSize) {
        this.selectedSize = selectedSize;
        return this;
    }

    public void setSelectedSize(String selectedSize) {
        this.selectedSize = selectedSize;
    }

    public String getColor() {
        return color;
    }

    public OrderedProduct color(String color) {
        this.color = color;
        return this;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPrice() {
        return price;
    }

    public OrderedProduct price(String price) {
        this.price = price;
        return this;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public OrderedProduct url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ProductType getProductType() {
        return productType;
    }

    public OrderedProduct productType(ProductType productType) {
        this.productType = productType;
        return this;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Order getOrder() {
        return order;
    }

    public OrderedProduct order(Order order) {
        this.order = order;
        return this;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderedProduct orderedProduct = (OrderedProduct) o;
        if (orderedProduct.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderedProduct.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrderedProduct{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", article='" + getArticle() + "'" +
            ", selectedSize='" + getSelectedSize() + "'" +
            ", color='" + getColor() + "'" +
            ", price='" + getPrice() + "'" +
            ", url='" + getUrl() + "'" +
            ", productType='" + getProductType() + "'" +
            "}";
    }
}
