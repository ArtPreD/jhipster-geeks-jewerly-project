package ua.com.geeks.jewerly.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    NEW, QUESTION_TO_SHOP, QUESTION_TO_CUSTOMER, AGREED, COMPILE, COMPILED, TRANSFERRED_TO_DELIVERY, DELIVERED, COMPLETED, RETURN, CANCELED
}
