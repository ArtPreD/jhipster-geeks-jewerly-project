package ua.com.geeks.jewerly.domain.enumeration;

/**
 * The DeliverType enumeration.
 */
public enum DeliverType {
    UKR_POST, NEW_POST
}
