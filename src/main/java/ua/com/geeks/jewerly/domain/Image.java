package ua.com.geeks.jewerly.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Image.
 */
@Entity
@Table(name = "image")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "image")
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "original_name")
    private String originalName;

    @Column(name = "url")
    private String url;

    @Column(name = "jhi_size")
    private Long size;

    @Column(name = "public_id")
    private String publicId;

    @Column(name = "version")
    private String version;

    @Column(name = "meta")
    private String meta;

    @Column(name = "main")
    private Boolean main;

    @ManyToOne
    @JsonIgnoreProperties("images")
    private Product product;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public Image originalName(String originalName) {
        this.originalName = originalName;
        return this;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getUrl() {
        return url;
    }

    public Image url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getSize() {
        return size;
    }

    public Image size(Long size) {
        this.size = size;
        return this;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getPublicId() {
        return publicId;
    }

    public Image publicId(String publicId) {
        this.publicId = publicId;
        return this;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getVersion() {
        return version;
    }

    public Image version(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMeta() {
        return meta;
    }

    public Image meta(String meta) {
        this.meta = meta;
        return this;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public Boolean isMain() {
        return main;
    }

    public Image main(Boolean main) {
        this.main = main;
        return this;
    }

    public void setMain(Boolean main) {
        this.main = main;
    }

    public Product getProduct() {
        return product;
    }

    public Image product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Image image = (Image) o;
        if (image.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), image.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Image{" +
            "id=" + getId() +
            ", originalName='" + getOriginalName() + "'" +
            ", url='" + getUrl() + "'" +
            ", size=" + getSize() +
            ", publicId='" + getPublicId() + "'" +
            ", version='" + getVersion() + "'" +
            ", meta='" + getMeta() + "'" +
            ", main='" + isMain() + "'" +
            "}";
    }
}
