package ua.com.geeks.jewerly.domain.enumeration;

/**
 * The CategoryType enumeration.
 */
public enum CategoryType {
    ROOT, PARENT, CHILD
}
