package ua.com.geeks.jewerly.domain.enumeration;

/**
 * The ColorType enumeration.
 */
public enum ColorType {
    GREEN, RED, BLACK, WHITE, ORANGE, BLUE, LAVANDA, YELLOW, AMETIST, CYAN, NONE
}
