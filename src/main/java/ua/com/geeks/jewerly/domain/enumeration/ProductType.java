package ua.com.geeks.jewerly.domain.enumeration;

/**
 * The ProductType enumeration.
 */
public enum ProductType {
    PETIT, PUSET, BRACELET, BROOCH, NECKLACE, SUSPENSION, PIERCING, RING, EARRINGS, SHARM, SPOON, KEYCHAIN, FIGURINE
}
