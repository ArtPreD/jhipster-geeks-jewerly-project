package ua.com.geeks.jewerly.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import ua.com.geeks.jewerly.domain.enumeration.ProductType;

import ua.com.geeks.jewerly.domain.enumeration.RockType;

import ua.com.geeks.jewerly.domain.enumeration.MaterialType;

import ua.com.geeks.jewerly.domain.enumeration.ColorType;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "product")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "product_type")
    private ProductType productType;

    @NotNull
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @NotNull
    @Column(name = "article", nullable = false, unique = true)
    private String article;

    @Column(name = "min_size")
    private Integer minSize;

    @Column(name = "max_size")
    private Integer maxSize;

    @Column(name = "sizes")
    private String sizes;

    @Enumerated(EnumType.STRING)
    @Column(name = "rock")
    private RockType rock;

    @Enumerated(EnumType.STRING)
    @Column(name = "material")
    private MaterialType material;

    @Enumerated(EnumType.STRING)
    @Column(name = "color")
    private ColorType color;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "available")
    private Boolean available;

    @NotNull
    @Column(name = "friendly_url", nullable = false, unique = true)
    private String friendlyUrl;

    @Column(name = "price")
    private Integer price;

    @Column(name = "meta")
    private String meta;

    @OneToMany(mappedBy = "product")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Image> images = new HashSet<>();
    @OneToMany(mappedBy = "product")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Comment> comments = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductType getProductType() {
        return productType;
    }

    public Product productType(ProductType productType) {
        this.productType = productType;
        return this;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getName() {
        return name;
    }

    public Product name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArticle() {
        return article;
    }

    public Product article(String article) {
        this.article = article;
        return this;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public Integer getMinSize() {
        return minSize;
    }

    public Product minSize(Integer minSize) {
        this.minSize = minSize;
        return this;
    }

    public void setMinSize(Integer minSize) {
        this.minSize = minSize;
    }

    public Integer getMaxSize() {
        return maxSize;
    }

    public Product maxSize(Integer maxSize) {
        this.maxSize = maxSize;
        return this;
    }

    public void setMaxSize(Integer maxSize) {
        this.maxSize = maxSize;
    }

    public String getSizes() {
        return sizes;
    }

    public Product sizes(String sizes) {
        this.sizes = sizes;
        return this;
    }

    public void setSizes(String sizes) {
        this.sizes = sizes;
    }

    public RockType getRock() {
        return rock;
    }

    public Product rock(RockType rock) {
        this.rock = rock;
        return this;
    }

    public void setRock(RockType rock) {
        this.rock = rock;
    }

    public MaterialType getMaterial() {
        return material;
    }

    public Product material(MaterialType material) {
        this.material = material;
        return this;
    }

    public void setMaterial(MaterialType material) {
        this.material = material;
    }

    public ColorType getColor() {
        return color;
    }

    public Product color(ColorType color) {
        this.color = color;
        return this;
    }

    public void setColor(ColorType color) {
        this.color = color;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Product categoryId(Long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Boolean isAvailable() {
        return available;
    }

    public Product available(Boolean available) {
        this.available = available;
        return this;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public String getFriendlyUrl() {
        return friendlyUrl;
    }

    public Product friendlyUrl(String friendlyUrl) {
        this.friendlyUrl = friendlyUrl;
        return this;
    }

    public void setFriendlyUrl(String friendlyUrl) {
        this.friendlyUrl = friendlyUrl;
    }

    public Integer getPrice() {
        return price;
    }

    public Product price(Integer price) {
        this.price = price;
        return this;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getMeta() {
        return meta;
    }

    public Product meta(String meta) {
        this.meta = meta;
        return this;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public Set<Image> getImages() {
        return images;
    }

    public Product images(Set<Image> images) {
        this.images = images;
        return this;
    }

    public Product addImages(Image image) {
        this.images.add(image);
        image.setProduct(this);
        return this;
    }

    public Product removeImages(Image image) {
        this.images.remove(image);
        image.setProduct(null);
        return this;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Product comments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public Product addComments(Comment comment) {
        this.comments.add(comment);
        comment.setProduct(this);
        return this;
    }

    public Product removeComments(Comment comment) {
        this.comments.remove(comment);
        comment.setProduct(null);
        return this;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Product product = (Product) o;
        if (product.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), product.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", productType='" + getProductType() + "'" +
            ", name='" + getName() + "'" +
            ", article='" + getArticle() + "'" +
            ", minSize=" + getMinSize() +
            ", maxSize=" + getMaxSize() +
            ", sizes='" + getSizes() + "'" +
            ", rock='" + getRock() + "'" +
            ", material='" + getMaterial() + "'" +
            ", color='" + getColor() + "'" +
            ", categoryId=" + getCategoryId() +
            ", available='" + isAvailable() + "'" +
            ", friendlyUrl='" + getFriendlyUrl() + "'" +
            ", price=" + getPrice() +
            ", meta='" + getMeta() + "'" +
            "}";
    }
}
