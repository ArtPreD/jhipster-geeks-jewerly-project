package ua.com.geeks.jewerly.domain.enumeration;

/**
 * The RockType enumeration.
 */
public enum RockType {
    CUBIC_ZIRCON, PEARLS, NONE
}
