package ua.com.geeks.jewerly.service.impl;

import ua.com.geeks.jewerly.service.OrderedProductService;
import ua.com.geeks.jewerly.domain.OrderedProduct;
import ua.com.geeks.jewerly.repository.OrderedProductRepository;
import ua.com.geeks.jewerly.repository.search.OrderedProductSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing OrderedProduct.
 */
@Service
@Transactional
public class OrderedProductServiceImpl implements OrderedProductService {

    private final Logger log = LoggerFactory.getLogger(OrderedProductServiceImpl.class);

    private final OrderedProductRepository orderedProductRepository;

    private final OrderedProductSearchRepository orderedProductSearchRepository;

    public OrderedProductServiceImpl(OrderedProductRepository orderedProductRepository, OrderedProductSearchRepository orderedProductSearchRepository) {
        this.orderedProductRepository = orderedProductRepository;
        this.orderedProductSearchRepository = orderedProductSearchRepository;
    }

    /**
     * Save a orderedProduct.
     *
     * @param orderedProduct the entity to save
     * @return the persisted entity
     */
    @Override
    public OrderedProduct save(OrderedProduct orderedProduct) {
        log.debug("Request to save OrderedProduct : {}", orderedProduct);
        OrderedProduct result = orderedProductRepository.save(orderedProduct);
        orderedProductSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the orderedProducts.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<OrderedProduct> findAll() {
        log.debug("Request to get all OrderedProducts");
        return orderedProductRepository.findAll();
    }


    /**
     * Get one orderedProduct by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrderedProduct> findOne(Long id) {
        log.debug("Request to get OrderedProduct : {}", id);
        return orderedProductRepository.findById(id);
    }

    /**
     * Delete the orderedProduct by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrderedProduct : {}", id);
        orderedProductRepository.deleteById(id);
        orderedProductSearchRepository.deleteById(id);
    }

    /**
     * Search for the orderedProduct corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<OrderedProduct> search(String query) {
        log.debug("Request to search OrderedProducts for query {}", query);
        return StreamSupport
            .stream(orderedProductSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
