package ua.com.geeks.jewerly.service;

import ua.com.geeks.jewerly.domain.OrderedProduct;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing OrderedProduct.
 */
public interface OrderedProductService {

    /**
     * Save a orderedProduct.
     *
     * @param orderedProduct the entity to save
     * @return the persisted entity
     */
    OrderedProduct save(OrderedProduct orderedProduct);

    /**
     * Get all the orderedProducts.
     *
     * @return the list of entities
     */
    List<OrderedProduct> findAll();


    /**
     * Get the "id" orderedProduct.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<OrderedProduct> findOne(Long id);

    /**
     * Delete the "id" orderedProduct.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the orderedProduct corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @return the list of entities
     */
    List<OrderedProduct> search(String query);
}
