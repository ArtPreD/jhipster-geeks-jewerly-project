package ua.com.geeks.jewerly.repository.search;

import ua.com.geeks.jewerly.domain.OrderedProduct;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the OrderedProduct entity.
 */
public interface OrderedProductSearchRepository extends ElasticsearchRepository<OrderedProduct, Long> {
}
