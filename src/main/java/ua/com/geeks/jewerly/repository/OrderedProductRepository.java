package ua.com.geeks.jewerly.repository;

import ua.com.geeks.jewerly.domain.OrderedProduct;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OrderedProduct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderedProductRepository extends JpaRepository<OrderedProduct, Long> {

}
