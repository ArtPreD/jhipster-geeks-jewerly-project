import { IProduct } from 'app/shared/model/product.model';

export interface IImage {
    id?: number;
    originalName?: string;
    url?: string;
    size?: number;
    publicId?: string;
    version?: string;
    meta?: string;
    main?: boolean;
    product?: IProduct;
}

export class Image implements IImage {
    constructor(
        public id?: number,
        public originalName?: string,
        public url?: string,
        public size?: number,
        public publicId?: string,
        public version?: string,
        public meta?: string,
        public main?: boolean,
        public product?: IProduct
    ) {
        this.main = this.main || false;
    }
}
