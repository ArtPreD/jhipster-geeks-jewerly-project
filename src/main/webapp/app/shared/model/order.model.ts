import { Moment } from 'moment';
import { IOrderedProduct } from 'app/shared/model/ordered-product.model';
import { IComment } from 'app/shared/model/comment.model';

export const enum DeliverType {
    UKR_POST = 'UKR_POST',
    NEW_POST = 'NEW_POST'
}

export const enum OrderStatus {
    NEW = 'NEW',
    QUESTION_TO_SHOP = 'QUESTION_TO_SHOP',
    QUESTION_TO_CUSTOMER = 'QUESTION_TO_CUSTOMER',
    AGREED = 'AGREED',
    COMPILE = 'COMPILE',
    COMPILED = 'COMPILED',
    TRANSFERRED_TO_DELIVERY = 'TRANSFERRED_TO_DELIVERY',
    DELIVERED = 'DELIVERED',
    COMPLETED = 'COMPLETED',
    RETURN = 'RETURN',
    CANCELED = 'CANCELED'
}

export interface IOrder {
    id?: number;
    customerName?: string;
    phone?: string;
    email?: string;
    address?: string;
    orderNumber?: number;
    createBy?: string;
    createDate?: Moment;
    modifyBy?: string;
    modifyDate?: Moment;
    deliverType?: DeliverType;
    trackNumber?: string;
    status?: OrderStatus;
    products?: IOrderedProduct[];
    comments?: IComment[];
}

export class Order implements IOrder {
    constructor(
        public id?: number,
        public customerName?: string,
        public phone?: string,
        public email?: string,
        public address?: string,
        public orderNumber?: number,
        public createBy?: string,
        public createDate?: Moment,
        public modifyBy?: string,
        public modifyDate?: Moment,
        public deliverType?: DeliverType,
        public trackNumber?: string,
        public status?: OrderStatus,
        public products?: IOrderedProduct[],
        public comments?: IComment[]
    ) {}
}
