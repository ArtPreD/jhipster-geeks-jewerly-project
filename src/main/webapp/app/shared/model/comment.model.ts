import { Moment } from 'moment';
import { IOrder } from 'app/shared/model/order.model';
import { IProduct } from 'app/shared/model/product.model';

export interface IComment {
    id?: number;
    message?: string;
    username?: string;
    submitDate?: Moment;
    isNew?: boolean;
    order?: IOrder;
    product?: IProduct;
}

export class Comment implements IComment {
    constructor(
        public id?: number,
        public message?: string,
        public username?: string,
        public submitDate?: Moment,
        public isNew?: boolean,
        public order?: IOrder,
        public product?: IProduct
    ) {
        this.isNew = this.isNew || false;
    }
}
