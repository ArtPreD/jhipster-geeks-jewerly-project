import { IImage } from 'app/shared/model/image.model';
import { ICategory } from 'app/shared/model/category.model';

export const enum CategoryType {
    ROOT = 'ROOT',
    PARENT = 'PARENT',
    CHILD = 'CHILD'
}

export interface ICategory {
    id?: number;
    name?: string;
    friendlyUrl?: string;
    description?: string;
    active?: boolean;
    meta?: string;
    parentId?: number;
    type?: CategoryType;
    image?: IImage;
    category?: ICategory;
    subcategories?: ICategory[];
}

export class Category implements ICategory {
    constructor(
        public id?: number,
        public name?: string,
        public friendlyUrl?: string,
        public description?: string,
        public active?: boolean,
        public meta?: string,
        public parentId?: number,
        public type?: CategoryType,
        public image?: IImage,
        public category?: ICategory,
        public subcategories?: ICategory[]
    ) {
        this.active = this.active || false;
    }
}
