import { IOrder } from 'app/shared/model/order.model';

export const enum ProductType {
    PETIT = 'PETIT',
    PUSET = 'PUSET',
    BRACELET = 'BRACELET',
    BROOCH = 'BROOCH',
    NECKLACE = 'NECKLACE',
    SUSPENSION = 'SUSPENSION',
    PIERCING = 'PIERCING',
    RING = 'RING',
    EARRINGS = 'EARRINGS',
    SHARM = 'SHARM',
    SPOON = 'SPOON',
    KEYCHAIN = 'KEYCHAIN',
    FIGURINE = 'FIGURINE'
}

export interface IOrderedProduct {
    id?: number;
    name?: string;
    article?: string;
    selectedSize?: string;
    color?: string;
    price?: string;
    url?: string;
    productType?: ProductType;
    order?: IOrder;
}

export class OrderedProduct implements IOrderedProduct {
    constructor(
        public id?: number,
        public name?: string,
        public article?: string,
        public selectedSize?: string,
        public color?: string,
        public price?: string,
        public url?: string,
        public productType?: ProductType,
        public order?: IOrder
    ) {}
}
