import { IImage } from 'app/shared/model/image.model';
import { IComment } from 'app/shared/model/comment.model';

export const enum ProductType {
    PETIT = 'PETIT',
    PUSET = 'PUSET',
    BRACELET = 'BRACELET',
    BROOCH = 'BROOCH',
    NECKLACE = 'NECKLACE',
    SUSPENSION = 'SUSPENSION',
    PIERCING = 'PIERCING',
    RING = 'RING',
    EARRINGS = 'EARRINGS',
    SHARM = 'SHARM',
    SPOON = 'SPOON',
    KEYCHAIN = 'KEYCHAIN',
    FIGURINE = 'FIGURINE'
}

export const enum RockType {
    CUBIC_ZIRCON = 'CUBIC_ZIRCON',
    PEARLS = 'PEARLS',
    NONE = 'NONE'
}

export const enum MaterialType {
    SILVER_925 = 'SILVER_925'
}

export const enum ColorType {
    GREEN = 'GREEN',
    RED = 'RED',
    BLACK = 'BLACK',
    WHITE = 'WHITE',
    ORANGE = 'ORANGE',
    BLUE = 'BLUE',
    LAVANDA = 'LAVANDA',
    YELLOW = 'YELLOW',
    AMETIST = 'AMETIST',
    CYAN = 'CYAN',
    NONE = 'NONE'
}

export interface IProduct {
    id?: number;
    productType?: ProductType;
    name?: string;
    article?: string;
    minSize?: number;
    maxSize?: number;
    sizes?: string;
    rock?: RockType;
    material?: MaterialType;
    color?: ColorType;
    categoryId?: number;
    available?: boolean;
    friendlyUrl?: string;
    price?: number;
    meta?: string;
    images?: IImage[];
    comments?: IComment[];
}

export class Product implements IProduct {
    constructor(
        public id?: number,
        public productType?: ProductType,
        public name?: string,
        public article?: string,
        public minSize?: number,
        public maxSize?: number,
        public sizes?: string,
        public rock?: RockType,
        public material?: MaterialType,
        public color?: ColorType,
        public categoryId?: number,
        public available?: boolean,
        public friendlyUrl?: string,
        public price?: number,
        public meta?: string,
        public images?: IImage[],
        public comments?: IComment[]
    ) {
        this.available = this.available || false;
    }
}
