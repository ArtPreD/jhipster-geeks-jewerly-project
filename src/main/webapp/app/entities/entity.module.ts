import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'order',
                loadChildren: './order/order.module#GeeksjewerlyOrderModule'
            },
            {
                path: 'ordered-product',
                loadChildren: './ordered-product/ordered-product.module#GeeksjewerlyOrderedProductModule'
            },
            {
                path: 'image',
                loadChildren: './image/image.module#GeeksjewerlyImageModule'
            },
            {
                path: 'comment',
                loadChildren: './comment/comment.module#GeeksjewerlyCommentModule'
            },
            {
                path: 'category',
                loadChildren: './category/category.module#GeeksjewerlyCategoryModule'
            },
            {
                path: 'product',
                loadChildren: './product/product.module#GeeksjewerlyProductModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GeeksjewerlyEntityModule {}
