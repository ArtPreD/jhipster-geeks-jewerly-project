import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOrderedProduct } from 'app/shared/model/ordered-product.model';

type EntityResponseType = HttpResponse<IOrderedProduct>;
type EntityArrayResponseType = HttpResponse<IOrderedProduct[]>;

@Injectable({ providedIn: 'root' })
export class OrderedProductService {
    public resourceUrl = SERVER_API_URL + 'api/ordered-products';
    public resourceSearchUrl = SERVER_API_URL + 'api/_search/ordered-products';

    constructor(protected http: HttpClient) {}

    create(orderedProduct: IOrderedProduct): Observable<EntityResponseType> {
        return this.http.post<IOrderedProduct>(this.resourceUrl, orderedProduct, { observe: 'response' });
    }

    update(orderedProduct: IOrderedProduct): Observable<EntityResponseType> {
        return this.http.put<IOrderedProduct>(this.resourceUrl, orderedProduct, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IOrderedProduct>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IOrderedProduct[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IOrderedProduct[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
