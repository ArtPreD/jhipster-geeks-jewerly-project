import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrderedProduct } from 'app/shared/model/ordered-product.model';

@Component({
    selector: 'jhi-ordered-product-detail',
    templateUrl: './ordered-product-detail.component.html'
})
export class OrderedProductDetailComponent implements OnInit {
    orderedProduct: IOrderedProduct;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ orderedProduct }) => {
            this.orderedProduct = orderedProduct;
        });
    }

    previousState() {
        window.history.back();
    }
}
