import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IOrderedProduct } from 'app/shared/model/ordered-product.model';
import { OrderedProductService } from './ordered-product.service';
import { IOrder } from 'app/shared/model/order.model';
import { OrderService } from 'app/entities/order';

@Component({
    selector: 'jhi-ordered-product-update',
    templateUrl: './ordered-product-update.component.html'
})
export class OrderedProductUpdateComponent implements OnInit {
    orderedProduct: IOrderedProduct;
    isSaving: boolean;

    orders: IOrder[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected orderedProductService: OrderedProductService,
        protected orderService: OrderService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ orderedProduct }) => {
            this.orderedProduct = orderedProduct;
        });
        this.orderService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IOrder[]>) => mayBeOk.ok),
                map((response: HttpResponse<IOrder[]>) => response.body)
            )
            .subscribe((res: IOrder[]) => (this.orders = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.orderedProduct.id !== undefined) {
            this.subscribeToSaveResponse(this.orderedProductService.update(this.orderedProduct));
        } else {
            this.subscribeToSaveResponse(this.orderedProductService.create(this.orderedProduct));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderedProduct>>) {
        result.subscribe((res: HttpResponse<IOrderedProduct>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackOrderById(index: number, item: IOrder) {
        return item.id;
    }
}
