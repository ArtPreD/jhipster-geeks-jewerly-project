import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { OrderedProduct } from 'app/shared/model/ordered-product.model';
import { OrderedProductService } from './ordered-product.service';
import { OrderedProductComponent } from './ordered-product.component';
import { OrderedProductDetailComponent } from './ordered-product-detail.component';
import { OrderedProductUpdateComponent } from './ordered-product-update.component';
import { OrderedProductDeletePopupComponent } from './ordered-product-delete-dialog.component';
import { IOrderedProduct } from 'app/shared/model/ordered-product.model';

@Injectable({ providedIn: 'root' })
export class OrderedProductResolve implements Resolve<IOrderedProduct> {
    constructor(private service: OrderedProductService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IOrderedProduct> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<OrderedProduct>) => response.ok),
                map((orderedProduct: HttpResponse<OrderedProduct>) => orderedProduct.body)
            );
        }
        return of(new OrderedProduct());
    }
}

export const orderedProductRoute: Routes = [
    {
        path: '',
        component: OrderedProductComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'geeksjewerlyApp.orderedProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: OrderedProductDetailComponent,
        resolve: {
            orderedProduct: OrderedProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'geeksjewerlyApp.orderedProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: OrderedProductUpdateComponent,
        resolve: {
            orderedProduct: OrderedProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'geeksjewerlyApp.orderedProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: OrderedProductUpdateComponent,
        resolve: {
            orderedProduct: OrderedProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'geeksjewerlyApp.orderedProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const orderedProductPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: OrderedProductDeletePopupComponent,
        resolve: {
            orderedProduct: OrderedProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'geeksjewerlyApp.orderedProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
