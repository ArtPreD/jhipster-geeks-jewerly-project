import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { GeeksjewerlySharedModule } from 'app/shared';
import {
    OrderedProductComponent,
    OrderedProductDetailComponent,
    OrderedProductUpdateComponent,
    OrderedProductDeletePopupComponent,
    OrderedProductDeleteDialogComponent,
    orderedProductRoute,
    orderedProductPopupRoute
} from './';

const ENTITY_STATES = [...orderedProductRoute, ...orderedProductPopupRoute];

@NgModule({
    imports: [GeeksjewerlySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        OrderedProductComponent,
        OrderedProductDetailComponent,
        OrderedProductUpdateComponent,
        OrderedProductDeleteDialogComponent,
        OrderedProductDeletePopupComponent
    ],
    entryComponents: [
        OrderedProductComponent,
        OrderedProductUpdateComponent,
        OrderedProductDeleteDialogComponent,
        OrderedProductDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GeeksjewerlyOrderedProductModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
