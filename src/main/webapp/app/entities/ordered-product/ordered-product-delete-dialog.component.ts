import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrderedProduct } from 'app/shared/model/ordered-product.model';
import { OrderedProductService } from './ordered-product.service';

@Component({
    selector: 'jhi-ordered-product-delete-dialog',
    templateUrl: './ordered-product-delete-dialog.component.html'
})
export class OrderedProductDeleteDialogComponent {
    orderedProduct: IOrderedProduct;

    constructor(
        protected orderedProductService: OrderedProductService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.orderedProductService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'orderedProductListModification',
                content: 'Deleted an orderedProduct'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ordered-product-delete-popup',
    template: ''
})
export class OrderedProductDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ orderedProduct }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(OrderedProductDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.orderedProduct = orderedProduct;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/ordered-product', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/ordered-product', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
