import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IOrderedProduct } from 'app/shared/model/ordered-product.model';
import { AccountService } from 'app/core';
import { OrderedProductService } from './ordered-product.service';

@Component({
    selector: 'jhi-ordered-product',
    templateUrl: './ordered-product.component.html'
})
export class OrderedProductComponent implements OnInit, OnDestroy {
    orderedProducts: IOrderedProduct[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        protected orderedProductService: OrderedProductService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected activatedRoute: ActivatedRoute,
        protected accountService: AccountService
    ) {
        this.currentSearch =
            this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search']
                ? this.activatedRoute.snapshot.params['search']
                : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.orderedProductService
                .search({
                    query: this.currentSearch
                })
                .pipe(
                    filter((res: HttpResponse<IOrderedProduct[]>) => res.ok),
                    map((res: HttpResponse<IOrderedProduct[]>) => res.body)
                )
                .subscribe((res: IOrderedProduct[]) => (this.orderedProducts = res), (res: HttpErrorResponse) => this.onError(res.message));
            return;
        }
        this.orderedProductService
            .query()
            .pipe(
                filter((res: HttpResponse<IOrderedProduct[]>) => res.ok),
                map((res: HttpResponse<IOrderedProduct[]>) => res.body)
            )
            .subscribe(
                (res: IOrderedProduct[]) => {
                    this.orderedProducts = res;
                    this.currentSearch = '';
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInOrderedProducts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IOrderedProduct) {
        return item.id;
    }

    registerChangeInOrderedProducts() {
        this.eventSubscriber = this.eventManager.subscribe('orderedProductListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
